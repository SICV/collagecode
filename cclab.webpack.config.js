const path = require('path');

module.exports = {
  entry: './src/cclab.js',
  mode: "production",
  output: {
    filename: 'cclab.js',
    path: path.resolve(__dirname, 'build'),
  },
  module: {
    rules: [
      { test: /\.js$/, use: ["source-map-loader"], enforce: "pre" },
      { test: /\.css$/, use: ['style-loader', 'css-loader'] },
      { test: /\.png$/, use: 'file-loader' },
    ]
  }
};