// Improved link targetting
// iframe with fragments ... map to resetting iframe.contentWindow.location.hash
// type="video/", type="audio/" ... map to video / audio elements

document.addEventListener("DOMContentLoaded", x=> {
    function parse_fragment (url) {
        // http://example.com/foo.mp4#t=01:00
        // => {baseURI: "http://example.com/foo.mp4", fragment: "t=01:00"}
        var m = url.match(/^(.*?)#(.*)$/),
            base = url,
            hash = '';
        if (m) {
            base = m[1];
            hash = m[2];    
        }
        return {
            base: base,
            hash: hash
        }
    }

    var TC_PATTERN = /(?:(\d\d):)?(\d\d):(\d\d)(?:[.,](\d{1,3}))?/;
    function timecode_to_seconds (tc) {
        var m = tc.match(TC_PATTERN);
        if (m) {
            return ((m[1] ? parseInt(m[1])*3600 : 0) + (parseInt(m[2])*60) + (parseInt(m[3])) + (m[4] ? parseFloat("0."+m[4]) : 0));
        }
    }
    function parseTime (h) {
        var m = h.match("t=([^&,]*)");
        // console.log("parseTime", h, m);
        if (m) { return timecode_to_seconds(m[1]); }
    }

    function iframe (iframe) {
        let that = {elt: iframe},
            cur_p = parse_fragment(iframe.contentWindow.location.toString());
        that.src = function (src) {
            console.log("iframe.src", src);
            var src_p = parse_fragment(src);
            console.log("iframe.srcp", src_p.base, cur_p.base, src_p.hash);
            if ((src_p.base == cur_p.base) && src_p.hash) {
                console.log("iframe setting hash", src_p.hash);
                iframe.contentWindow.location.hash = src_p.hash;
            } else {
                console.log("iframe setting full location", src);
                iframe.contentWindow.location = src;
            }
        }
        return that;
    }

    function mediaelt (video) {
        let that = {elt: video},
            cur_p = parse_fragment(video.currentSrc);
        that.src = function (src) {
            console.log("media.src", src);
            var src_p = parse_fragment(src);
            var time = parseTime(src_p.hash);
            if ((src_p.base == cur_p.base) && time) {
                console.log("video.setting time", time);
                video.currentTime = time;
                video.play();
            } else {
                console.log("video.setting full src", src);
                video.src = src;
                video.play();
            }
            cur_base = src.base;
        }
        return that;
    }


    function find_target (a) {
        let a_type = a.getAttribute("type"),
            a_target_name = a.getAttribute("target"),
            targets = document.querySelectorAll(`[name="${a_target_name}"]`),
            by_node_name = {};

        if (targets.length == 0 && DEBUG) {
            console.log("mediatagets no match for target_name", a_target_name);
        }
        for (let i=0, tlen=targets.length; i<tlen; i++) {
            if (DEBUG) { console.log("mediatargets: targets[i]", targets[i]); }
            by_node_name[targets[i].nodeName] = targets[i];
        }
        console.log("bnn", a_target_name, by_node_name);
        if (a_type && a_type.startsWith("video/")) {
            if (by_node_name["VIDEO"]) { 
                return mediaelt(by_node_name["VIDEO"]);
            } else if (by_node_name["AUDIO"]) {
                return mediaelt(by_node_name["AUDIO"]);
            }
        } else if (a_type && a_type.startsWith("audio/")) {
            if (by_node_name["AUDIO"]) { 
                return mediaelt(by_node_name["AUDIO"]);
            } else if (by_node_name["VIDEO"]) {
                return mediaelt(by_node_name["VIDEO"]);
            }
        } else {
            if (by_node_name["VIDEO"]) { 
                return mediaelt(by_node_name["VIDEO"]);
            } else if (by_node_name["AUDIO"]) {
                return mediaelt(by_node_name["AUDIO"]);
            }            
        }
        if (by_node_name["IFRAME"]) {
            return iframe(by_node_name["IFRAME"]);
        } else {
            console.log(`mediatargets: warning: no target matching ${a_target_name}`);
        }
    }
    var DEBUG = true;
    // Test this with leaflet iframe ...
    document.addEventListener("click", e => {
        if (DEBUG) { console.log("mediatargets.click", e); }
        if (e.target.nodeName == "A" && e.target.target) {
            let a = e.target,
                a_target_name = a.target,
                target = find_target(a);
                // target_location = parse_fragment(target.contentWindow.location.toString());

            if (DEBUG) { console.log("mediatargets.click", a_target_name, target); }
            // HERE the "ccframe" abstraction !!!
            target.src(a.href);
            e.preventDefault();
            // console.log("a", a, a.href);
            // console.log("a_href", a_href);
            // console.log("target", target_location);
             
            // if (target && a_href.base == target_location.base) {

            //     // console.log(`MATCH setting hash to ${a_href.hash}`);

            //     target.contentWindow.location.hash = a_href.hash;
            // }

        }
    })
})