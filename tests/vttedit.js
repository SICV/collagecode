document.addEventListener("DOMContentLoaded", e => {
    console.log("DOMCL");

formats = {
    "html5": {
        alwaysFract: true,
        alwaysHours: false
    },
}
function zeropadend (x, places) {
    while (x.length < places) {
        x += "0";
    }
    return x;
}
function seconds_to_timecode(s, format) {
    format = format ? formats[format] : formats["html5"];
    var hh = Math.floor(s/3600),
        mm, ss, ff, ret;
    s -= (hh*3600);
    mm = Math.floor(s/60);
    s -= (mm*60);
    ss = Math.floor(s)
    ff = s - ss;
    ret = '';
    if (hh || format.alwaysHours) {
        ret += ((hh < 10) ? "0" : "") + hh + ":";
    }
    ret += ((mm < 10) ? "0" : "") + mm + ":";
    ret += ((ss < 10) ? "0" : "") + ss;
    if (ff > 0 || format.alwaysFract) {
        ret += "." + zeropadend(ff.toFixed(3).substring(2, 5), 3);
    }
    return ret;
}

function waitUntilReady (track) {
    return new Promise (resolve => {
        function poll () {
            if (track.readyState >=2) {
                resolve(track.readyState);
            } else {
                console.log("waiting on track", track.readyState);
                window.setTimeout(poll, 250);
            }            
        }
        window.setTimeout(poll, 0);
    })
}

function output_vtt (track) {
    var text = "WEBVTT\n\n",
        cue;
    var tc = seconds_to_timecode;
    for (var i=0, l=track.cues.length; i<l; i++) {
        cue = track.cues[i];
        text += `${tc(cue.startTime)} --> ${tc(cue.endTime)}\n`;
        text += `${cue.text}\n\n`;
    }
    return text;
}

function post (url, data, done, error) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            // Request finished. Do processing here.
            done(xhr);
        } else {
            if (error) error(xhr);
        }
    }
    xhr.send(data);
}

    var video = document.querySelector("audio,video");
    video.addEventListener("durationchange", e => go(video));
    console.log('video', video);
    function go (video) {
        var track_elt = video.querySelector("track"),
            track = track_elt.track;
        window.track = track;
        window.video = video;


        waitUntilReady(track_elt).then(result => {
            console.log("TRACK READY")
            var divs_by_id = {};

            function writeback () {
                for (var ci=0, cl=track.cues.length; ci<cl; ci++) {
                    let cue = track.cues[ci];
                    cue.text = divs_by_id[cue.id].querySelector(".body").textContent;
                }
            }

            document.addEventListener("keydown", e=> {
                // console.log("keydown", e);
                if (e.key == 's' && e.ctrlKey) {
                    console.log("SAVE");
                    e.preventDefault();
                    writeback();
                    var vtt = output_vtt(track);
                    /// console.log("save", vtt);
                    post(track_elt.src, "text="+encodeURIComponent(vtt), e => {
                        console.log("saved");
                    })
                }

                if (e.keyCode == 37 && e.ctrlKey) {
                    e.preventDefault();
                    video.currentTime = video.currentTime - 5;
                } else if (e.keyCode == 39 && e.ctrlKey) {
                    e.preventDefault();
                    video.currentTime = video.currentTime + 5;
                } else if (e.keyCode == 38 && e.ctrlKey) {
                    video.paused ? video.play() : video.pause();
                    e.preventDefault();
                } else if (e.keyCode == 40 && e.ctrlKey) {
                }
            })

            if (result != 2) {
                console.log("track load error");
                return;
            }
            track.addEventListener("cuechange", (e, x) => {
                // console.log("cuechange", e, track.activeCues);
                var cc = div.querySelectorAll(".activecue");
                for (var i=0, l=cc.length; i<l; i++) {
                    cc[i].classList.remove("activecue");
                }
                for (var i=0; i<track.activeCues.length; i++) {
                    let cue = track.activeCues[i];
                    if (cue.id && divs_by_id[cue.id]) {
                        divs_by_id[cue.id].classList.add("activecue");
                    }
                }
            })
            var div = document.createElement("div");
            div.classList.add("captions");
            div.setAttribute("contenteditable", "true");
            document.body.appendChild(div);
            console.log("track.cues.length", track.cues.length);

            // https://gist.github.com/gleuch/2475825
/*            function cue_to_html_src (c) {
                var df = c.getCueAsHTML(),
                    div = document.createElement("div");
                div.appendChild(df.cloneNode(true));
                return div.innerHTML;
            }
*/

            for (var ci=0, cl=track.cues.length; ci<cl; ci++) {
                let cue = track.cues[ci],
                    d = document.createElement("div"),
                    a = document.createElement("a"),
                    body = document.createElement("div"),
                    tc_start = seconds_to_timecode(cue.startTime);
                cue.id = ci;
                divs_by_id[cue.id] = d;
                body.classList.add("body");
                //d.setAttribute("contenteditable", "true");
                d.classList.add("cue");
                a.href = video.currentSrc + "#t=" + tc_start;
                a.setAttribute("target", "audio");
                a.setAttribute("type", "audio/mp3");
                a.textContent = tc_start;
                track.cues[ci].text;
                d.appendChild(a);
                d.appendChild(body);
                body.appendChild(track.cues[ci].getCueAsHTML().cloneNode(true));
                div.appendChild(d);
            }
       })
 
    }
})