import * as href from './href.js';
import { MultiPlayer } from './multiplayer.js'

/*
design decisions:
* make (media) fragments a key/natural part
*/

export class Playlist {
	constructor (opts) {
		this.elt = document.querySelector(opts.elt);
		this.items = opts.items;
		this.interupter = null;
		this.interupter_reject = null;
	}
	interupt () {
		if (this.interupter_reject) {
			this.interupter_reject(new Error("playlistinterupt"));
		}
	}
	async play () {
		this.interupter = new Promise((resolve, reject) => {
			this.interupter_reject = reject;
		});
		var items = this.items;
		// var yt = await initYouTube();
		// console.log("inited youtube");
		// var p = new YouTubePlayer();
		// var p2 = new HTML5Player();
		var p = new MultiPlayer();
		// console.log("ytplayer ready");
		try {

			for (var i=0, l=items.length; i<l; i++) {
				var item = items[i],
					thehref = item.href,
					h = href.cc_href(thehref);
					// ytid = YouTubePlayer.extract_youtube_id(h.base);
				// console.log("ytid", ytid);
				item.classList.add("active");
				var result = await Promise.race([this.interupter, p.play_from_to(this.elt, h.base, h.start, h.end)]);
				console.log("playlist.result", result);
				item.classList.remove("active");

				// if (ytid) {
				// } else {
				// 	await p2.play_from_to(this.elt, h.base, h.start, h.end);
				// }

			}


		} catch (e) {
			console.log("playlist.exeception", e.message);
		} finally {
			console.log("playlist.finally");
			for (var i=0, l=items.length; i<l; i++) {
				var item = items[i];
				item.classList.remove("active");
			}
		}
	}
}

// export class FlexiPlaylist {
// 	constructor (opts) {
// 		this.elt = document.querySelector(opts.elt);
// 		// this.items_selector = opts.items || "a";
// 	}
// 	set_items (items) {
// 		this.items = items;
// 	}
// 	play (index_or_item) {

// 	}
// }
