
var formats = {
	"html5": {
		alwaysFract: true
	},
}
function zeropadend (x, places) {
	while (x.length < places) {
		x += "0";
	}
	return x;
}

export function seconds_to_timecode(s, format) {
	format = format ? formats[format] : formats["html5"];
	var hh = Math.floor(s/3600),
		mm, ss, ff, ret;
	s -= (hh*3600);
	mm = Math.floor(s/60);
	s -= (mm*60);
	ss = Math.floor(s)
	ff = s - ss;
	ret = ((hh < 10) ? "0" : "") + hh + ":";
	ret += ((mm < 10) ? "0" : "") + mm + ":";
	ret += ((ss < 10) ? "0" : "") + ss;
	if (ff > 0 || format.alwaysFract) {
		ret += "." + zeropadend(ff.toString().substring(2, 5), 3);
	}
	return ret;
}

export var TC_PATTERN = /(?:(\d\d):)?(\d\d):(\d\d)(?:[.,](\d{1,3}))?/;
export function timecode_to_seconds (tc) {
	var m = tc.match(TC_PATTERN);
	if (m) {
		return ((m[1] ? parseInt(m[1])*3600 : 0) + (parseInt(m[2])*60) + (parseInt(m[3])) + (m[4] ? parseFloat("0."+m[4]) : 0));
	}
}

