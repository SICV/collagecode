import * as href from './href.js';


function sleep(delay) {
    return new Promise(function(resolve) {
        setTimeout(resolve, delay);
    });
}


// https://developers.google.com/youtube/iframe_api_reference#Playback_status
// player.seekTo(time, seekAhead) : seekAhead -- when true slower in that it can go into new ranges
// pauseVideo, playVideo, (stopVideo), mute, unMute, setVolume, getVolume, isMuted, 
// getPlayerState: -1 unstarted, 0 ended, 1 playing, 2 paused, 3 buffering, 5 video cued 
// getDuration
// getIframe
// addEventListener, removeEventListener
// destroy
class YouTubePlayer {
	static extract_id (url) {
		var pat = /https?:\/\/(youtu\.be\/(.+))|(www\.youtube\.com\/watch\?v=(.+))$/,
			match = pat.exec(url);
		return match ? match[2] || match[4] : null;
	}

	static init_youtube_api () {
		if (!window.onYouTubeIframeAPIReady_promise) {
			window.onYouTubeIframeAPIReady_promise = new Promise(function (resolve) {
				window.onYouTubeIframeAPIReady = function () {
					// console.log("youtube_api: ready");
					resolve();
				}
				// console.log("youtube_api: loading...");
				var tag = document.createElement('script');
			    tag.src = "https://www.youtube.com/iframe_api";
		      	var firstScriptTag = document.getElementsByTagName('script')[0];
		      	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
			});
		}
		return window.onYouTubeIframeAPIReady_promise;
	}

	constructor () {}

	init_player_p (tmp) {
		return new Promise(resolve => {
			this.player = new YT.Player(tmp, {
				events: {
		            'onReady': function (x) {
		            	// init a poller for 
		            	resolve();
		            },
		            'onStateChange': x => {
		            	this.state = x.data;
		            	// console.log("yt: state", this.state);
		            }
		          }
		        });			
		})		
	}
	async init_player (elt) {
		// the indicated element is REPLACED by an iframe, thus tmp
		await YouTubePlayer.init_youtube_api();
		var tmp = document.createElement("div");
		elt.appendChild(tmp);
		await this.init_player_p(tmp);
	}
	async wait_for_state (state) {
		while (1) {
			if (this.state == state) {
				return;
			}
			// console.log("wait_for_state", this.state, state);
			await sleep(250);
		}
	}
	async play_from_to (elt, url, start, end) {
		this.id = YouTubePlayer.extract_id(url);
		if (!this.player) {
			await this.init_player(elt);
		}
		this.player_elt = this.player.getIframe();
		this.player.loadVideoById(this.id, start || 0);
		this.wait_for_state(1);
		this.player.getIframe().style.display = "block";
		while (1) {
			var ct = this.player.getCurrentTime();
			// console.log("ct", ct);
			if (ct >= end) {
				this.player.pauseVideo();
				break;
			}
			await sleep(250);
		}
	}
	hide () {
		if (!this.player_elt) return;
		this.player_elt.style.display = "none";
	}
	show () {
		if (!this.player_elt) return;
		this.player_elt.style.display = "block";
	}
}

class HTML5Player {
	constructor () {}
	async wait_for_event (event) {
		// console.log("waiting for event", event);
		return new Promise(resolve => {
			var el = e => {
				// console.log("received event", event);
				this.player.removeEventListener(event, el);
				resolve();
			};
			this.player.addEventListener(event, el);			
		})
	}
	async play_until (time) {
		return new Promise(resolve => {
			var timeupdate = e => {
					if (this.player.currentTime >= time) {
						this.player.pause();
						finish();
					}
				},
				ended = e => {
					finish();					
				},
				finish = () => {
					this.player.removeEventListener("timeupdate", timeupdate);			
					this.player.removeEventListener("ended", ended);
					resolve();
				};
			this.player.addEventListener("timeupdate", timeupdate);			
			this.player.addEventListener("ended", ended);			
			this.player.play();
		})
	}
	async seekTo (time) {
		this.player.currentTime = time;
		return await this.wait_for_event("canplaythrough");
	}
	async play_from_to (elt, url, start, end) {
		if (!this.player) {
			this.player = document.createElement("video");
			this.player.setAttribute("controls", "controls");
			elt.appendChild(this.player);
			this.player.style.display = "none";
		}
		if (this.current_src !== url) {
			this.current_src = url;
			this.player.src = url;
		}
		await this.seekTo(start);
		this.show();
		await this.play_until(end);
	}
	hide () {
		if (!this.player) return;
		this.player.style.display = "none";
	}
	show () {
		if (!this.player) return;
		this.player.style.display = "block";
	}
}


// https://developer.vimeo.com/player/sdk/reference#about-player-methods
class VimeoPlayer {

	static extract_id (url) {
		var pat = /https?:\/\/vimeo\.com\/(\d+)$/,
			match = pat.exec(url);
		return match ? match[1] : null;
	}

	// <script src="https://player.vimeo.com/api/player.js"></script>
	static init_vimeo_api () {
		if (!window.init_vimeo_api_promise) {
			window.init_vimeo_api_promise = new Promise(function (resolve) {
				var script = document.createElement('script');
				script.onload = function () {
					// console.log("init_vimeo_api_promise.loaded", Vimeo.Player);
					resolve();
				}
				script.src = "https://player.vimeo.com/api/player.js"
			    var firstScriptTag = document.getElementsByTagName('script')[0];
		      	firstScriptTag.parentNode.insertBefore(script, firstScriptTag);
			});
		}
		return window.init_vimeo_api_promise;
	}

	constructor () {}

	async play_from_to (elt, url, start, end) {
		await VimeoPlayer.init_vimeo_api();
		var id = VimeoPlayer.extract_id(url);
		if (!this.player_elt) {
			var tmp = document.createElement("div");
			this.player_elt = tmp;
			elt.appendChild(tmp);
			this.player = new Vimeo.Player(tmp, {id: id});
		} else {
			if (this.current_url !== url) {
				await this.player.loadVideo(id);
			}
		}
		this.current_url = url;
		await this.player.setCurrentTime(start);
		this.player.play();
		while (1) {
			var ct = await this.player.getCurrentTime();
			if (ct >= end) {
				this.player.pause();
				break
			}
			// console.log("vimeo.ct", ct);
			sleep(250);
		}
	    // video01Player.on('play', function() {
	    //   console.log('Played the first video');
	    // });
	}
	hide () {
		if (!this.player_elt) return;
		this.player_elt.style.display = "none";
	}
	show () {
		if (!this.player_elt) return;
		this.player_elt.style.display = "block";
	}
}

export class MultiPlayer {
	constructor () {
		this.youtube_player = new YouTubePlayer();
		this.vimeo_player = new VimeoPlayer();
		this.html5_player = new HTML5Player();
		this.allplayers = [this.youtube_player, this.vimeo_player, this.html5_player];
		this.player = null;
	}
	get_player (url) {
		var id = YouTubePlayer.extract_id(url);
		if (id) { return this.youtube_player; }
		id = VimeoPlayer.extract_id(url);
		if (id) { return this.vimeo_player; }
		return this.html5_player;
	}
	show_only (player) {
		this.allplayers.forEach(x => {
			(x === player) ? x.show() : x.hide();
		})
	}
	async play_from_to (elt, url, start, end) {
		this.player = this.get_player(url);
		this.show_only(this.player);
		await this.player.play_from_to(elt, url, start, end);
	}
}
