---
title: Playlist examples
---
<style>
	a.active {
		background: black;
		color: white;
	}
	#examples {
		width: 50%;
		float: left;
	}
</style>
<div id="examples">
* [ward#t=01:00,01:10]
* [ward_yt#t=01:10,01:20]
* [ward_vimeo#t=01:20,01:30]
* [ward#t=01:30,01:35]
* [ward_yt#t=01:35,01:40]
* [ward_vimeo#t=01:40,01:45]
* [ward#t=01:45,02:00]
* [ward_yt#t=02:00,02:15]
* [ward_vimeo#t=02:15,02:30]
</div>

* [ward#t=04:00,04:05]
* [ward#t=02:00,02:05]
* [4b was the p][klepp#t=35:15,35:25]
* [this is a (very) nice sentence][greenspan#t=02:00,02:09]
* [Media Events viewer](http://automatist.org/2017/05/media_events.html)


[wikipage]: https://commons.wikimedia.org/w/index.php?title=File%3AWard_Cunningham%2C_Inventor_of_the_Wiki.webm
[ward]: https://upload.wikimedia.org/wikipedia/commons/transcoded/3/31/Ward_Cunningham%2C_Inventor_of_the_Wiki.webm/Ward_Cunningham%2C_Inventor_of_the_Wiki.webm.360p.vp9.webm
[ward_vimeo]: https://vimeo.com/96237918
[ward_yt]: https://www.youtube.com/watch?v=XqxwwuUdsp4
[klepp]: https://youtu.be/yCcWpzY8dIA
[greenspan]: https://www.youtube.com/watch?v=bV2JGpS-1R0

<div id="playerdiv" style="border: 1px solid blue">
<div id="player"></div>
</div>

<script src="dist/playlist.js"></script>
<script>
var items = document.querySelectorAll("#examples a"),
	p = new playlist.Playlist({elt: "#player", items: items});
items = Array.prototype.slice.apply(items);
items.forEach(x => {
	x.addEventListener("click", e => {
		p.interupt();
		e.preventDefault();
	})
})
p.play();
</script>