// rollup.config.js
// https://github.com/rollup/rollup-plugin-commonjs
// npm install rollup rollup-plugin-commonjs rollup-plugin-node-resolve
import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';

export default [{
  input: 'src/playlist.js',
  output: {
    file: 'dist/playlist.js',
    format: 'iife',
    name: 'playlist'
  },
  plugins: [
    resolve(), 
    commonjs()
  ]
}];