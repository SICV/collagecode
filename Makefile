mdsrc=$(shell find . -iname "*.md")
html_from_md=$(mdsrc:%.md=%.html)

all: $(html_from_md)

# print-variables
print-%:
	@echo '$*=$($*)'

.PHONY: dist
dist:
	cp build/* dist
	cp node_modules/plyr/dist/plyr.css dist


###################################################
# cc server (collagecode/)
###################################################
clean:
	rm -rf collagecode.egg-info dist build

update_dev:
	python3 -m pip install --user --upgrade setuptools wheel


copy-editor:
	cp -r cceditor/dist collagecode/data/htdocs/cceditor

install: dist/cceditorapp.js
	pip install -e .

###################################################

ccvideo: dist/ccvideo.js
	cp dist/ccvideo.js ../lgm2010/lib


dist/cceditorapp.js: src/*.js src/editor.css
	node_modules/.bin/webpack --config editor.webpack.js
	cp -r cceditor/dist collagecode/data/htdocs/cceditor

# dist/gltest.js: src/gltest.js 
# 	node_modules/.bin/webpack --config webpack.test.js

dist/ccplayer.js: src/*.js
	node_modules/.bin/webpack --config player.webpack.js

dist/ccvideo.js: src/*.js
	node_modules/.bin/webpack --config ccvideo.webpack.js

dist/ccyoutube.js: src/*.js
	node_modules/.bin/webpack --config ccyoutube.webpack.js

%.html: %.md
	pandoc --from markdown \
		--to html \
		--standalone \
		--css style.css \
		$< -o $@


%.cc.html: %.cc.md
	scripts/expand_reference_links.py --input $< \
	| \
	pandoc --from markdown \
		--to html \
		--standalone \
		-o $@

# June 2020
tests/saskia_willaerts_0156.captions.html: tests/saskia_willaerts_0156.vtt
	cat tests/head.html > $@
	python scripts/srt_to_html.py --input $< \
		--href saskia_willaerts_0156.mp3 \
		--target "audio" \
		--type "audio/mp3" >> $@
	cat tests/foot.html >> $@


# TST
%/titles.html: %/titles.md
	scripts/expand_reference_links.py --input $< \
	| \
	pandoc --from markdown \
	--to html \
	--standalone \
	-o $@

%/titles.srt.html: %/titles.md scripts/html2srt.py
	scripts/expand_reference_links.py --input $< \
	| \
	pandoc --from markdown \
	--to html \
	--standalone \
	| \
	scripts/html2srt.py --ids \
	| \
	scripts/symboliclinks.py \
	| \
	scripts/html5tidy \
	--stylesheet "../lib/srt.css" \
	--script "../lib/rv-srt.js" \
	--output $@ 

%/video.mp4 %/edit.srt.html: %/titles.srt.html
	scripts/edit.py --input $< \
	--output $*/edit.srt.html \
	--outputvideo ../$*/video.mp4 --performedit \
	--cwd edit

# For updating the titles *without* regenerating the video
# this rule is useful
# %/video.mp4 %/edit.srt.html: %/titles.srt.html
#       scripts/edit.py --input $< \
#               --output $*/edit.srt.html \
#               --outputvideo ../$*/video.mp4 \
#               --cwd edit
#       touch $*/video.mp4




