function classcycler (button, target, modes) {
    var ret = {};
    var i = 0;
    function set_index () {
        var l; // nb changes i in classcycler scope
        for (i=0, l=modes.length; i<l; i++) {
            if (target.classList.contains(modes[i])) { return; }
        }
    }
    button.addEventListener("click", function () {
        set_index();
        console.log("i", i);
        target.classList.replace(modes[i], modes[i = (i+1) % modes.length]);
    });
    ret.set_mode = function (m) {
        set_index();
        target.classList.replace(modes[i], m);
    }
    return ret;
}
// classcycler(gebi("toggle"), gebi("content"), "splitscreen overlay".split(" "));
