import os
import setuptools

def find (p, d):
    ret = []
    for b, dd, ff in os.walk(os.path.join(p, d)):

        for f in ff:
            if not f.startswith("."):
                fp = os.path.join(b, f)
                ret.append(os.path.relpath(fp, p))
    ret.sort()
    # for x in ret[:10]:
    #     print "**", x
    return ret

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="collagecode",
    version="1.0.0",
    author="Michael Murtaugh",
    author_email="mm@automatist.org",
    description="Make & Serve",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.constantvzw.org/SICV/collagecode",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
    package_data={'collagecode': find("collagecode", "data/")},
    entry_points={
        'console_scripts': [
            'collagecode = collagecode:main',
        ]
    },
    python_requires='>=3.6',
    install_requires=['aiohttp'],
)
