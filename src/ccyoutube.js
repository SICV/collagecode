// YouTube API wrapper element
// Following:
// https://developers.google.com/youtube/iframe_api_reference

function loadyoutubeiframeapi() {
    return new Promise(function(resolve) {
      var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      window.onYouTubeIframeAPIReady = function () {
        resolve();
      }
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    });
}

var namepat = /^https?:\/\/www\.youtube\.com\/watch\?v=(.*)$/;

class YouTubeVideo {
  constructor (elt) {
    this.elt = elt;
    this.youtube_id = 'M7lc1UVf-VE';
    this._playing = false;
    this.player = null;
  }
  async init () {
    console.log("init", this.youtube_id);
    if (typeof (YT) == "undefined") await loadyoutubeiframeapi();
    var player_elt = document.createElement("div");
    this.elt.appendChild(player_elt);
    this.player = new YT.Player(player_elt, {
          height: '390',
          width: '640',
          playerVars: { 'autoplay': 1, 'controls': 0 },
          events: {
            'onReady': this.onPlayerReady.bind(this),
            // 'onStateChange': this.onPlayerStateChange.bind(this)
          }
    });
  }
  onPlayerReady () {
    console.log("youtube:playerready");
    this.player.cueVideoById(this.youtube_id, 0);
    if (this._playing) {
      // this.play();
    }
  }
  onPlayerStateChange () {
    console.log("youtube:playerstatechange");
  }
  get currentTime () {
    return this.player.getCurrentTime();
  }
  set currentTime (t) {
    this.player.seekTo(t, true);
  }
  get duration () {
    return this.player.getDuration();
  }
  set src (src) {
    var m = namepat.exec(src);
    console.log("src", m);
    if (m != null) {
      this.youtube_id = m[1];
      this.init();
    } else {
      console.log("YouTubeVideo: src not recognized", src);
    }
  }
  play () {
    this._playing = true;
    if (this.player) {
      console.log("youtubevideo: calling player.playVideo")
      this.player.playVideo();
    }
  }
  pause () {
    if (this.player) {
      this.player.pauseVideo();
    }
  }
}

      // function onPlayerReady(event) {
      //   event.target.playVideo();
      // }

      // // 5. The API calls this function when the player's state changes.
      // //    The function indicates that when playing a video (state=1),
      // //    the player should play for six seconds and then stop.
      // var done = false;
      // function onPlayerStateChange(event) {
      //   if (event.data == YT.PlayerState.PLAYING && !done) {
      //     setTimeout(stopVideo, 6000);
      //     done = true;
      //   }
      // }
      // function stopVideo() {
      //   player.stopVideo();
      // }


module.exports = {
  YouTubeVideo: YouTubeVideo
}