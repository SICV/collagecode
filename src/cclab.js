/* Fresh start 2021 */
import { CommandRegistry } from '@lumino/commands';
import { Message } from '@lumino/messaging';
import { BoxPanel, SplitPanel, CommandPalette, ContextMenu, DockPanel, Menu, MenuBar, Widget } from '@lumino/widgets';
import '../style/index.css';

// import '@fortawesome/fontawesome-free/css/all.min.css';
import { URLBar } from './urlbar.js';
import { EditorWidget } from './editor.js';
import { FrameWidget } from './frame.js';
// import { ListingWidget } from './listing.js';
import * as reflinks from './reflinks.js';

// import * as ticdb from './ticdb.js';
const VIDEO_URL = "http://vandal.ist/thesituationisttimes/video/2017-12-14/MVI_0033.web.mp4";

const _cc = {};
window._cc = _cc;
const commands = new CommandRegistry();

function parse_fragment (url) {
    // console.log("parse_fragment", url);
    var ret = {}
    if (url.indexOf("#") >= 0) {
        var p = url.split("#", 2);
        ret.base = p[0];
        ret.fragment = p[1];
    } else {
        ret.base = url;
        ret.fragment = '';
    }
    return ret;
}

_cc.handle_link = function (href, target) {
    // HANDLE HREF + TARGET
    console.log("_cc.handle_link", href, target);
    if (_cc.frame) {
      _cc.frame.src = href;
    }
    /*
    if (href.match(/\.md$/) && !target)  {
        // EDITOR LINK
        // console.log("editor link");
        open_in_editor(href);
    } else {
        // OTHER LINKS
        target = target || "link";

        var linksstack = that.layout.root.getItemsById("linkstack");
        if (linksstack.length > 0) {
            linksstack = linksstack[0];
            // console.log("got linksstack", linksstack);
            // look for matching target
            var t = that.find_frame_by_target(target);
            if (t) {
                var f = frames_by_target[target];
                if (f) {
                    // console.log("cceditorapp: using ccframe", f);
                    f.src = href;
                    // maybe only if necessary (now grabs focus?!)
                    t.parent.setActiveContentItem(t);
                } else {
                    console.log("cceditorapp: no ccframe in frames_by_target for target", target);
                }
            } else {
                linksstack.addChild({
                    type: 'component',
                    componentName: 'ccframe',
                    componentState: { href: href, target: target}
                });
            }
            // if 
        } else {
            // Create the linkstack with a ccframe targeting this href
            var root_row = that.layout.root.contentItems[0];
            root_row.addChild({
                type: 'column',
                content: [{
                    type: 'stack',
                    id: 'linkstack',
                    content: [{
                        type: 'component',
                        componentName: 'ccframe',
                        componentState: { href: href, target: target}
                    }]
                }]
            });
        }
        // console.log("click", href, target);
        // var p = frames_by_target[target]; 
        // if (p) { p.src = href; }                            
    }
    */
}

_cc.paste_link = function () {
  var href = urlbar.url,
    hash = urlbar.fragment;
  console.log("paste_link", href, hash);
  if (_cc.editor) {
    var ev = _cc.editor.getValue();
    var thereflinks = reflinks.extract_reflink_definitions(ev);
    if (hash) {
        href = parse_fragment(href).base + hash;
    }
    var compact_href = reflinks.compactRefLink(href, thereflinks);
    if (compact_href !== href) {
      _cc.editor.replaceSelection("["+compact_href+"]");
    } else {
      _cc.editor.replaceSelection("<"+href+">");      
    }
  }
}

_cc.setActiveEditor = (editor) => {
  if (editor !== _cc.editor) {
    // console.log("setActiveEditor", editor);
    _cc.editor = editor;    
  }
}
_cc.hashchange = (baseurl, fragment) => {
  urlbar.url=baseurl;
  urlbar.fragment = fragment;
}

let urlbar;

function main () {
  const dialog = document.getElementById("dialog"),
    openfile = document.getElementById("openfile"),
    openfile_fileinput = document.getElementById("openfile_fileinput"),
    openfile_cancel = document.getElementById("openfile_cancel");

  function dialog_is_open () {
    return dialog.style.display != "none";
  }

  function close_dialog() {
    dialog.style.display = "none";
    openfile.style.display = "none";
    openfile_fileinput.value = null;
  }

  function show_openfile() {
    dialog.style.display = "flex";
    openfile.style.display = "block";
    openfile_fileinput.focus();
  }

  openfile_cancel.addEventListener("click", e=> {
    close_dialog();
  })

  commands.addCommand('annotation:new', {
    label: 'New',
    mnemonic: 1,
    iconClass: 'fa fa-file',
    execute: () => {
      // console.log('Import...');
      // show_openfile();
      let editor = new EditorWidget('Annotation');
      dock.addWidget(editor);
    }
  });

  commands.addCommand('annotation:save', {
    label: 'Save',
    mnemonic: 1,
    iconClass: 'fa fa-save',
    execute: () => {
      console.log('Save');
      // show_openfile();
    }
  });

  commands.addCommand('annotation:save-as', {
    label: 'Save As...',
    mnemonic: 1,
    iconClass: 'fa fa-save',
    execute: () => {
      console.log('Save As...');
      // show_openfile();
    }
  });

  commands.addCommand('media:toggle', {
    label: 'Play/Pause',
    mnemonic: 1,
    iconClass: 'fa fa-play',
    execute: () => {
      console.log('Play/Pause media...');
      // show_openfile();
      _cc.frame.toggle();
    }
  });

  commands.addCommand('media:step-back', {
    label: 'Step back',
    mnemonic: 2,
    iconClass: 'fa fa-step-backward',
    execute: () => {
      console.log('Media Back...');
      _cc.frame.jumpback();
    }
  });

  commands.addCommand('media:step-forward', {
    label: 'Step forward',
    mnemonic: 3,
    iconClass: 'fa fa-step-forward',
    execute: () => {
      console.log('Media Forward...');
      _cc.frame.jumpforward();
    }
  });

  commands.addCommand('cc:pastefragment', {
    label: 'Paste fragment',
    mnemonic: 1,
    iconClass: 'fa fa-paste',
    execute: () => {
      console.log('Paste fragment...');
      _cc.paste_link();
    }
  });

  commands.addKeyBinding({
    keys: ['Accel O'],
    selector: 'body',
    command: 'tic:new'
  });

  commands.addKeyBinding({
    keys: ['Accel S'],
    selector: 'body',
    command: 'annotation:save'
  });

  commands.addKeyBinding({
    keys: ['Accel Shift ArrowUp'],
    selector: 'body',
    command: 'media:toggle'
  });

  commands.addKeyBinding({
    keys: ['Accel Shift ArrowLeft'],
    selector: 'body',
    command: 'media:step-back'
  });

  commands.addKeyBinding({
    keys: ['Accel Shift ArrowRight'],
    selector: 'body',
    command: 'media:step-forward'
  });

  commands.addKeyBinding({
    keys: ['Accel Shift ArrowDown'],
    selector: 'body',
    command: 'cc:pastefragment'
  });

  // commands.addKeyBinding({
  //   keys: ['Accel O'],
  //   selector: 'body',
  //   command: 'tic:open'
  // });

  let filemenu = new Menu({ commands });
  filemenu.addItem({ command: 'annotation:new' });
  filemenu.addItem({ command: 'annotation:save' });
  filemenu.addItem({ command: 'annotation:save-as' });
  // filemenu.addItem({ type: 'separator' });

  filemenu.title.label = 'Annotation';
  filemenu.title.mnemonic = 0;

  let mediamenu = new Menu({ commands });
  mediamenu.addItem({ command: 'media:toggle' });
  mediamenu.addItem({ command: 'media:step-back' });
  mediamenu.addItem({ command: 'media:step-forward' });
  mediamenu.addItem({ command: 'cc:pastefragment' });
  // filemenu.addItem({ type: 'separator' });

  mediamenu.title.label = 'Media';
  mediamenu.title.mnemonic = 0;

  let bar = new MenuBar();
  bar.addMenu(filemenu);
  bar.addMenu(mediamenu);
  bar.id = 'menuBar';

  urlbar = new URLBar();
  urlbar.id = "urlbar";

  document.addEventListener('keydown', (event) => {
    // console.log("keydown", event);
    if (event.key == "Escape" && dialog_is_open()) {
      close_dialog();
    } else {
      commands.processKeydownEvent(event);

    }
  });

  // let listing = new ListingWidget('Listing');
  // window._tic.listing = listing;


  let dock = new DockPanel();
  // dock.addWidget(editor2, { ref: editor1 });
  dock.id = 'dock';

  let frame = new FrameWidget(); // VIDEO_URL
  _cc.frame = frame;
  dock.addWidget(frame);

  let editor = new EditorWidget('Annotation');
  // window._tic.editor = editor;
  dock.addWidget(editor, { mode: 'split-right' }); // { mode: 'split-right' }
  editor.setValue(`<http://vandal.ist/thesituationisttimes/video/2017-12-14/MVI_0033.web.mp4>\n\n<https://www.youtube.com/watch?v=162VzSzzoPs>`);


  BoxPanel.setStretch(dock, 1);

  let main = new SplitPanel({ direction: 'left-to-right', spacing: 0 });
  main.id = 'main';
  // main.addWidget(palette);
  // main.addWidget(listing);
  main.addWidget(dock);

  window.onresize = () => { main.update(); };

  Widget.attach(bar, document.body);
  Widget.attach(urlbar, document.body);
  Widget.attach(main, document.body);
}

window.onload = main;
