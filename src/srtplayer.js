var createIntervalTree = require("interval-tree-1d")
// var Subtitle = require("subtitle");
var srt = require("./sloppysrt.js");


function get (url, callback) {
	var request = new XMLHttpRequest();
	request.open('GET', url, true);
	request.onload = function() {
	  if (request.status >= 200 && request.status < 400) {
	    callback(null, request.responseText);
	  } else {
	  	callback("server error", null);
	  }
	};
	request.onerror = function() {
	  callback("connection error", null);
	};
	request.send();
}

function updateset () {
	var that = {},
		active_ids = {},
		actives = [];

	function copy (obj) {
		var ret = {};
		for (var attr in obj) {
        	if (obj.hasOwnProperty(attr)) ret[attr] = obj[attr];
    	}
    	return ret;
	}

	function update (newitems, callbacks) {
		// console.log("update", newitems.map(function (x) { return x.id }));
		var exit_ids = copy(active_ids),
			enter = [],
			hold = [],
			change = false;

		for (var i=0, l=newitems.length; i<l; i++) {
			var ni = newitems[i];
			if (active_ids[ni.id] != undefined) {
				// hold
				if (callbacks && callbacks.hold) {
					callbacks.hold.call(that, ni);
				}
				delete exit_ids[ni.id];
			} else {
				// enter
				change = true;
				active_ids[ni.id] = ni;
				if (callbacks && callbacks.enter) {
					callbacks.enter.call(that, ni);
				}
			}
		}
		for (var id in exit_ids) {
			// exit
			change = true;
			if (exit_ids.hasOwnProperty(id)) {
				delete active_ids[id];
				if (callbacks && callbacks.exit) {
					callbacks.exit.call(that, exit_ids[id]);
				}
			}
		}

		if (change) {
			actives = [];		
			for (var id in active_ids) {
				if (active_ids.hasOwnProperty(id)) {
					actives.push(active_ids[id]);
				}
			}
			if (callbacks && callbacks.change) {
				callbacks.change.call(that, actives);
			}			
		}
	}
	that.update = update;
	return that;
}

function srtplayer (video, elt, srt_src_href) {
	var ttid = 0,
		byid = {},
		active = updateset();

	get(srt_src_href, function (err, content) {
		// console.log("srt", content);
		console.log("srt", srt);
		var tt = srt.parseSRT(content),
			tree = createIntervalTree();
		srt.normalizeSRT(tt);
		for (var i=0, l=tt.length; i<l; i++) {
			var w = (tt[i].end != undefined) ? tt[i].end - tt[i].start : 10;
			// console.log("insert", tt[i].start, w)
			tt[i].id = ++ttid;
			byid[tt.id] = document.createElement("div");
			var intv = [tt[i].start, (tt[i].end != undefined) ? tt[i].end : tt[i].start + 10];
			intv.title = tt[i];
			tree.insert(intv);
		}
		video.addEventListener("timeupdate", function () {
			var items = [];
			tree.queryPoint(this.currentTime, function (x) {
				items.push(x.title);
			});
			active.update(items, {
			// active.update(rt.search({x: this.currentTime, y: 0, w: 0.001, h: 10}), {
				change: function (items) {
					// console.log("change", items);
					var text = "";
					for (var i=0, l=items.length; i<l; i++) {
						text += items[i].body;
					}
					elt.innerHTML = text;
				}
			});
		}, false);

	})
}

module.exports.srtplayer = srtplayer;

