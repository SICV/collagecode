// var GoldenLayout = require("golden-layout");

// static
// import * as GoldenLayout from "golden-layout";
// console.log("GoldenLayout", GoldenLayout);

// dynamic
import (/* webpackChunkName: "golden-layout" */"golden-layout").then((module) => {
    console.log("module", module);
})

// async function getComponent () {
//     const element = document.createElement("div");
//     const { default: codemirror } = await import("codemirror");
//     console.log("getComponent", codemirror);
//     return codemirror;
// }


// var codemirror = getComponent();
