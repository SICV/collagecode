var timecode = require("./timecode.js"),
    EventEmitter = require("eventemitter3");

function parse_fragment (url) {
    // console.log("parse_fragment", url);
    ret = {}
    if (url.indexOf("#") >= 0) {
        var p = url.split("#", 2);
        ret.base = p[0];
        ret.fragment = p[1];
    } else {
        ret.base = url;
        ret.fragment = '';
    }
    return ret;
}

function parse_times (h) {
    var m = h.match("t=([^&,]*)(,([^&,]*))?");
    // console.log("parseTime", h, m);
    if (m) {
        var ret = { start: timecode.timecode_to_seconds(m[1]) };
        if (m[3]) {
            ret['end'] = timecode.timecode_to_seconds(m[3]);
        }
        return ret;
    }
    return {};
}

class CCVideo {
    constructor (elt) {
        // console.log("ccframe.init", elt);
        if (elt) {
            this.elt = elt;
        } else {
            this.elt = document.createElement("video");
        }
        this.start = null;
        this.end = null;
        this.did_fragmentend = false;
        var src = this.elt.getAttribute("src");
        if (src) {
            var purl = parse_fragment(src);
            this.url = src;
        }
        this.elt.addEventListener("load", this._load.bind(this));
        this.elt.addEventListener("timeupdate", this._timeupdate.bind(this)); 
        this.elt.addEventListener("ended", this._ended.bind(this)); 
        this.events = new EventEmitter();
    }
    on (event, callback) {
        this.events.on(event, callback);
        return this;
    }
    set src (url) {
        // console.log("ccframe.src", url);
        var purl = parse_fragment(url),
            t = parse_times(purl.fragment),
            previous_url = this.url ? parse_fragment(this.url) : null;

        this.url = url;
        if (!previous_url || previous_url.base != purl.base) {
            this.events.emit("srcchange", purl.base);
        }
        if (previous_url && previous_url.base == purl.base) {
            if (t.start !== undefined) {
                console.log("ccvideo: update currentTime + play", t.start);
                this.did_fragmentend = false;
                this.start = t.start;
                this.end = t.end;
                this.elt.currentTime = t.start;
                this.elt.play();
            } else {
                console.log("ccvideo: ignoring bad timecode", purl.fragment);
            }
        } else {
            console.log("ccvideo: setting src", url);
            this.elt.src = purl.base;
            if (t.start !== undefined) {
                console.log("ccvideo: setting initial time", t.start);
                this.did_fragmentend = false;
                this.start = t.start;
                this.end = t.end;
                this.elt.currentTime = t.start;
            }
            // need to ensure ready before playing!
            this.elt.play();
        }
        return this;
    }

    get src () {
        return this.url;
    }

    get base () {
        return parse_fragment(this.url).base;
    }

    _load (e) {}

    _update_hash (f) {
        this.url = parse_fragment(this.url).base+f;
        // console.log("updated hash", this.url);
    }

    _timeupdate (e) {
        var ct = this.elt.currentTime,
            h = "#t="+timecode.seconds_to_timecode(ct);
        if (!this.did_fragmentend && this.end && ct>=this.end) {
            this.did_fragmentend = true;
            this.events.emit("fragmentend");
        }
        this.events.emit("hashchange", h, this.elt.currentTime);
        // new: may 2018, update my own source to reflect changed hash!
        this._update_hash(h);
    }

    _ended (e) {
        this.events.emit("ended");
    }

    pause () {
        this.elt.pause();
    }
    play () {
        this.elt.play();
    }

    /* Functions for Ctrl Keys */
    toggle () {
        this.elt.paused ? this.elt.play() : this.elt.pause();
    }

    jumpforward () {
        this.elt.currentTime += 5;
    }

    jumpback () {
        this.elt.currentTime -= 5;
    }

    get currentTime () {
        return this.elt.currentTime;
    }
    set currentTime (t) {
        this.elt.currentTime = t;
    }
    
    get fragment () {
        // if (this.pageNumber !== null) {
        //     return "#page="+this.pageNumber;
        if (this.elt.currentTime) {
            return "#t="+timecode.seconds_to_timecode(this.elt.currentTime);
        } else {
            return this.elt.contentWindow.location.hash;
        }
    }
    set fragment (val) {
        
    }
    
}

module.exports = CCVideo;
