import { Widget } from '@lumino/widgets';

import * as CodeMirror from 'codemirror';
import 'codemirror/mode/markdown/markdown.js';
// import 'codemirror/mode/lua/lua.js';
import 'codemirror/mode/javascript/javascript.js';
import 'codemirror/addon/fold/foldcode.js';
import 'codemirror/addon/fold/foldgutter.js';
// import 'codemirror/lib/codemirror.css';
// import 'codemirror/theme/monokai.css';
import * as reflinks from "./reflinks.js";

const CM_OPTS = {
        mode: {'name': 'markdown', 'fold': true},
        // mode: {'name': 'lua'},
        theme: 'default',
        lineNumbers: true,
        lineWrapping: true,
        // extraKeys: {"Ctrl-S": function(cm){ save(); }},
        foldGutter: true,
        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]    
    };

function cm_markdown_link_element_of_interest(elt) {
    // given a clicked element, return the "interesting" element if considered a markdown link
    // In a nutshell, always isolate the cm-url except in the case of an implicit reference link (e.g. See [my website][]. )
    if (elt.classList.contains("cm-link")) {
        // check if followed by cm-url & use that (unless blank)
        if (elt.nextSibling && 
            elt.nextSibling.classList &&
            elt.nextSibling.classList.contains("cm-url") &&
            elt.nextSibling.textContent != "[]" ) {
            return elt.nextSibling;    
        }
        return elt;
    } else if (elt.classList.contains("cm-url")) {
        if (elt.textContent == "[]" &&
            elt.previousSibling &&
            elt.previousSibling.classList &&
            elt.previousSibling.classList.contains("cm-link")) {
            return elt.previousSibling;
        }
        return elt;
    }
}

export class EditorWidget extends Widget {

  static createNode() {
    let node = document.createElement('div');
    let editor = document.createElement('div');

    // let input = document.createElement('input');
    // input.placeholder = 'Placeholder...';
    // content.appendChild(input);
    node.appendChild(editor);
    return node;
  }

  constructor(name) {
    super({ node: EditorWidget.createNode() });
    // let editor_div = this.node.getElementsByTagName('div')[0];
    this.cm = CodeMirror(this.editorDiv, CM_OPTS);

    var nop = function (cm) {};
    // block shift-ctrl-up to prevent shift-up + cc keys 
    this.cm.setOption("extraKeys", {
      "Shift-Ctrl-Up": nop,
      "Shift-Ctrl-Down": nop,
      "Shift-Ctrl-Left": nop,
      "Shift-Ctrl-Right": nop,
    });
    this.editorDiv.addEventListener("click", this._click.bind(this));
    // window.code = this.cm;
    this.setFlag(Widget.Flag.DisallowLayout);
    this.addClass('content');
    this.addClass(name.toLowerCase());
    this.title.label = name;
    this.title.closable = true;
    this.title.caption = `Editor: ${name}`;
  }

  get editorDiv () {
    return this.node.getElementsByTagName('div')[0];
  }

  get inputNode() {
    return this.node.getElementsByTagName('input')[0];
  }
  setValue (text) {
    this.cm.setValue(text);
    // this.cm.update();
  }
  getValue () {
    return this.cm.getValue();
  }
  replaceSelection (text) {
      this.cm.replaceSelection(text);
  }

  onResize () { // this fires initially
    // console.log("editor.onResize");
    this.cm.refresh();
  }
  onAfterAttach () {
    // console.log("editor.onAfterAttach", this, "hidden", this.isHidden);
    if (!this.isHidden) {
      // initial state is this
      window._cc.setActiveEditor(this);
    }
  //   this.cm.refresh();
  }
  onAfterShow () {
    // console.log("editor.onAfterShow", this, this.isHidden);
    window._cc.setActiveEditor(this);
  //   this.cm.refresh();
  }
  // onAfterHide () {}
  // onUpdateRequest(Message) {}
  onActivateRequest(Message) {
    // console.log("editor.onActivateRequest", this, Message);
    if (this.isAttached) {
      // this.inputNode.focus();
      this.cm.refresh();
      this.cm.focus();
    }
  }

  _click (e) {
      var elt = e.target;
      var href, target;
      // (src, classList)
      // console.log("_click", elt);
      elt = cm_markdown_link_element_of_interest(elt);
      if (elt) {
          var ref = elt.textContent,
              m = ref.match(/\[(.+?)\]\:?/);
          if (m) {
              ref = m[1].toLowerCase();
              // console.log("reflink", ref);              
              var ev = this.getValue();
              var thereflinks = reflinks.extract_reflink_definitions(ev);
              var reflink = reflinks.xpandRefLink(ref, thereflinks);
              if (reflink) {
                  href = reflink.href;
                  target = reflink.title;
              } else {
                  console.log("warning: undefined reference in link", ref);
              }
          } else {
              // trim ( ) and < > forms
              ref = ref.replace(/^\((.+)\)$/, "$1");
              ref = ref.replace(/^\<(.+)\>$/, "$1");
              // console.log("link", ref);
              m = ref.match(/^(.+?)(?: *(?:\"(.+)\")?)$/);
              href = m[1];
              target = m[2] || "";
          }
      }
      if (href) {
          _cc.handle_link(href, target);
      }
  }


}
