Revisiting [earlier documents](CollageCode_Timelines.html), returning to the idea of representing transformations between different frames of reference in a generalized way of *flattening* graphs of (fragmented) links.

Key idea of rendering dynamic views by imagining a cursor or screen position as dynamically linking onto a canvas, and performing all transformations by *flattening* the links to be "direct" from final surface to media.


In considering rendering image collages in a leaflet tile system:

Tile (x, y, z)
+------------+
|0,0 256x256 |
|            |
|            |
+------------+

tile#x,y,z <--> tile#x2,y2,z2

Tiles, by the nature of their conception, have implicit relationship between themselves.
Namely: tile 0,0,0 and tile 0,0,1 are linked such that tile 0,0,1 could be said to overlap the upper-left quadrant of tile 0,0,0.

tile0,0,1 <--> canvas#0,0,128x128

canvas#-100,-100,200x200 <--> image_full#0,0 1920x2709
canvas#-100,-100,200x200 <--> image_thumb#0,0 192x271

or using a image object to hold different resolutions of the same image...

canvas#-100,-100,200x200 <--> image
image#0,0 200x200 <--> image_full#0,0 1920x2709
image#0,0 200x200 <--> image_thumb#0,0 192x271

