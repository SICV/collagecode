#!/usr/bin/env python3

import html5lib
import argparse, sys, re
from urllib.parse import urlparse
from xml.etree import ElementTree as ET
from urllib.parse import urlparse, urlunparse
from mediafragment import parse_temporal_fragment
import subprocess
from math import floor
from timecode import timecode_fromsecs


def is_cut (d):
    for e in d.iter():
        text = ''
        if e.text != None:
            text = e.text.lower()
        if e.tag == "strong" and "cut" in text:
            return True
    return False

def parse_href (href):
    p = urlparse(href)
    base = urlunparse((p.scheme, p.netloc, p.path, None, None, None))
    mft = parse_temporal_fragment(p.fragment)
    return base, mft

def process_tree (t):
    # produce an index
    edl = []
    cut = []
    for elt in t.findall(".//div[@data-href]"):
        href = elt.attrib.get("data-href")
        href, intv = parse_href(href)
        d = {}
        d['href'] = href
        d['start'] = intv.start
        d['end'] = intv.end
        d['elt'] = elt

        # close any open interval
        # NB: d might be a cut element, but it still functions to close active edits!
        if len(edl) and edl[-1]['end'] == None and d['start']:
            # print ("closing open interval", d['start'])
            edl[-1]['end'] = d['start']
        if not is_cut(elt):
            edl.append(d)
        else:
            cut.append(d)
    return edl, cut

def compact (edl):
    out = []
    l = len(edl)
    for i, d in enumerate(edl):
        if len(out) and d['href'] == out[-1]['href'] and (out[-1]['end'] == None or out[-1]['end'] == d['start']):
            out[-1]['end'] = d['end']
        else:
            out.append(dict(d))
    return out

def to_cmd (eld, framerate):
    def s2f (t, framerate=args.framerate):
        if t == None:
            return None
        return floor(framerate*t)
    def cmdify (edit):
        if edit['end']:
            return '"{0}" in={1} out={2}'.format(edit['href'], s2f(edit['start']), s2f(edit['end']))
        else:
            return '"{0}" in={1}'.format(edit['href'], s2f(edit['start']))
    return "melt " + " ".join([cmdify(edit) for edit in edl])    

def get_xml (cmd):
    out = subprocess.check_output(cmd+" -consumer xml", cwd="edit", shell=True)
    out = out.decode("utf-8")
    with open ("edit.xml", "w") as f:
        print (out, file=f)
    return ET.fromstring(out)

def html5timecode (s):
    return timecode_fromsecs(s, fractdelim=".", alwayshours=True)

def rewrite_edl (edl, xml, newpath, framerate, title):
    playlist = xml.find(".//playlist")
    entries = playlist.findall(".//entry")
    assert (len(entries) == len(edl))
    cur_frame = 0
    for d, entry in zip(edl, entries):
        cur_time = cur_frame / framerate
        eout, ein = int(entry.attrib.get("out")), int(entry.attrib.get("in"))
        # print ("out={0} in={1} cur_frame:{2}, cur_time: {3}/{4}".format(eout, ein, cur_frame, cur_time, html5timecode(cur_time)), file=sys.stderr)
        eframes =  eout - ein + 1
        cur_frame += eframes
        newhref = newpath + "#t=" + html5timecode(cur_time)
        elt = d['elt']
        elt.attrib['data-href'] = newhref
        a = elt.find(".//a[@title=\"{0}\"]".format(title))
        if a != None:
            a.attrib['href'] = newhref
    # REMOVE CUTS

def parentchilditer (elt):
    for parent in elt.iter():
        for child in parent:
            yield parent, child

def remove_elt (t, elt):
    for p, c in parentchilditer(t):
        if c == elt:
            p.remove(elt)
            return True

if __name__ == "__main__":
    ap = argparse.ArgumentParser("transform an srthtml with multiple sources into an EDL to edit to a single source + new HTMLSRT relative to this edit")
    ap.add_argument("--input", type=argparse.FileType('r'), default=sys.stdin)
    ap.add_argument("--output", type=argparse.FileType('w'), default=sys.stdout)
    ap.add_argument("--outputvideo", default="../edit.mp4")
    ap.add_argument("--framerate", type=float, default=25.0)
    ap.add_argument("--title", default="video")
    ap.add_argument("--performedit", action="store_true", default=False)
    ap.add_argument("--cwd", default="edit")
    # ap.add_argument("--profile", default="vcd_pal")
    ap.add_argument("--profile", default="quarter_pal_wide")
    # ap.add_argument("--ids", action="store_true", default=False, help="add numerical ids to title divs")
    args = ap.parse_args()
    t = html5lib.parse(args.input, namespaceHTMLElements=False, treebuilder="etree")
    edl, cuts = process_tree(t)

    # REMOVE THE CUTS from the HTML
    for c in cuts:
        remove_elt(t, c['elt'])

    # debug output
    # for i, d in enumerate(edl):
    #     print (i, d)

    # edl = compact(edl)
    cmd = to_cmd(edl, args.framerate)

    xml = get_xml(cmd)
    rewrite_edl(edl, xml, args.outputvideo, args.framerate, title=args.title)
    print (ET.tostring(t, method="html", encoding="unicode"), file=args.output)

    if args.performedit:
        cmd += " -profile {0} -consumer avformat:{1}".format(args.profile, args.outputvideo)
        print ("Performing the edit", file=sys.stderr)
        print (cmd, file=sys.stderr)
        subprocess.check_output(cmd, cwd="edit", shell=True)

#    print (get_xml(to_cmd(edl, args.framerate)))
