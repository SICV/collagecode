#!/usr/bin/env python3
# from __future__ import print_function
import argparse
import re, sys

"""

Expand reference links, with special support for hash/fragments.

"""

link_definition = re.compile(r'^\[(?P<label>[^\]]+)\]:\s+(?P<url>[^ \n]+)(?:\s+(?P<title>(?:\"(.*)?\")|(?:\'(.*)?\')|(?:\((.*)?\))))?$', re.M)
# reflink = re.compile(r'\[([^\]\n]+)\]\[([^\]\n]*)\]')
reflink_shortcutrefs = re.compile(r'(?:\[([^\]\n]+)\]\[([^\]\n]*)\])|(?:\[([^\]\n]+)\](?!:))')

def expand_reference_links (text, hash_only=False):
    link = re.compile(r"\s")

    link_definitions = {}
    for m in link_definition.finditer(text):
        v = m.groupdict()
        if v['title']:
            groups = m.groups()
            v['title'] = groups[3] or groups[4] or groups[5]
        link_definitions[v['label'].lower()] = v
        # print ("link definition {}: {{url: {}, title: {}}}".format(v['label'].lower(), v['url'], v['title']), file=sys.stderr)

    def sub (m):
        gg = m.groups()
        # print ("got {0} groups, {1}".format(len(gg), gg), file=sys.stderrd)
        if len(gg) == 2 or gg[2] == None:
            label, ref = gg[0], gg[1]
            if ref == '':
                ref = label
        else:
            label = gg[2]
            ref = gg[2]

        # print ("*", (label, ref), m.group(0), file=sys.stderr)
        if not hash_only or '#' in ref:
            if "#" in ref:
                ref, hsh = ref.split('#', 1)
            else:
                ref, hsh = ref, ''
            lref = ref.lower()
            if lref in link_definitions:
                v = link_definitions.get(lref)
                if hsh:
                    if v['title']:
                        return "[{0}]({1}#{2} \"{3}\")".format(label, v['url'], hsh, v['title'])
                    else:
                        return "[{0}]({1}#{2})".format(label, v['url'], hsh)
                else:
                    if v['title']:
                        return "[{0}]({1} \"{2}\")".format(label, v['url'], v['title'])
                    else:
                        return "[{0}]({1})".format(label, v['url'])
            # Split "#" from reference and
            # turn [label][reference#hash] => [label](URL#hash)

        # OTHERWISE RETURN ORIGINAL UNCHANGED
        return m.group(0)

    return reflink_shortcutrefs.sub(sub, text)

if __name__ == "__main__":

    ap = argparse.ArgumentParser("")
    ap.add_argument("--input", type=argparse.FileType('r'), default=sys.stdin)
    ap.add_argument("--output", type=argparse.FileType('w'), default=sys.stdout)
    ap.add_argument("--hash-only", action="store_true", default=False)
    args = ap.parse_args()
    print (expand_reference_links(args.input.read(), hash_only=args.hash_only), file=args.output)


