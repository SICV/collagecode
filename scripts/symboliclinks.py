#!/usr/bin/env python3

from html5lib import parse
import argparse, sys
from xml.etree import ElementTree as ET


ap = argparse.ArgumentParser("Replace links with fragments with css-stylable symbols")
inf = ap.add_argument("--input", type=argparse.FileType("r"), default=sys.stdin)
outf = ap.add_argument("--output", type=argparse.FileType("w"), default=sys.stdout)
outf = ap.add_argument("--label", default="", help="replace link labels with this text, default blank")
args = ap.parse_args()

t = parse(args.input, namespaceHTMLElements=False)
count = 0
c2 = 0
for elt in t.iter():
    if elt.tag == "a":
        href = elt.attrib.get("href")
        if href:
            # print ("link", href, elt.text  )
            count += 1
            if "#" in elt.text:
                elt.text = args.label
                title = elt.attrib.get("title")
                if title:
                    klass = elt.attrib.get("class", "")
                    if klass:
                        klass += " "
                    klass += "symbol "
                    klass += title.replace(" ", "_")
                    elt.attrib['class'] = klass
                # print ("LINK", href)
                c2 += 1
# print (count, c2) 

print (ET.tostring(t, method="html", encoding="unicode"))