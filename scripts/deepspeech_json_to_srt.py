import json, sys
import argparse
from timecode import timecode_fromsecs

def tc (s):
    return timecode_fromsecs(s, fractdelim=".", alwaysfract=True)

def transcript_to_titles (t, max_title_duration, max_gap, gap_titles=True):
    cur_start_time = None
    curwords = []
    titles = []

    words = [w for w in t['words'] if w['word']]

    for i, w in enumerate(words):
        if cur_start_time == None:
            cur_start_time = w['start_time ']
        curwords.append(w['word'])

        gap = 0
        if i+1<len(words):
            next_start = words[i+1]['start_time ']
            next_word = words[i+1]
            cur_start = w['start_time ']
            gap = next_start - cur_start - w['duration']

        if (w['start_time '] >= cur_start_time + max_title_duration) or (gap > max_gap):
            titles.append((cur_start_time, curwords))
            if gap_titles and (gap > max_gap):
                titles.append((cur_start_time+w['duration'], []))

            cur_start_time = None
            curwords = []

    if curwords:
        titles.append((cur_start_time, curwords))

    return titles

def titles_to_srt (titles, file):
    for i, (start, words) in enumerate(titles):
        if i+1<len(titles):
            nt = titles[i+1]
        else:
            nt = None
        if nt:
            print ("{} --> {}".format(tc(start), tc(nt[0])), file=file)
        else:
            print ("{} --> {}".format(tc(start), tc(start+10.0)), file=file)
        print (" ".join(words), file=file)
        print(file=file)

if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("--input", type=argparse.FileType("r"), default=sys.stdin)
    ap.add_argument("--output", type=argparse.FileType("w"), default=sys.stdout)
    ap.add_argument("--transcript", type=int, default=1)
    ap.add_argument("--max-title-duration", type=float, default=10.0)
    ap.add_argument("--max-gap", type=float, default=1.0)
    ap.add_argument("--vtt", action="store_true", default=False)
    args = ap.parse_args()
    data = json.load(args.input)

    t = data['transcripts'][args.transcript-1]
    titles = transcript_to_titles(t, args.max_title_duration, args.max_gap, gap_titles=True)
    # print (json.dumps(titles, indent=2), file=args.output)
    if args.vtt:
        print ("WEBVTT\n", file=args.output)
    titles_to_srt(titles, args.output)
