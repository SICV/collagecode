#!/usr/bin/env python3

import html5lib
import argparse, sys, re, json
from urllib.parse import urlparse
from xml.etree import ElementTree as ET

def extract_markers (t, title):
    titles = []
    cur_title = None
    for elt in t.iter():
        if elt.tag == "div" and "title" in elt.attrib.get("class", ""):
            cur_title = elt
        elif elt.tag == "a" and elt.attrib.get("title", "") == args.title:
            href = elt.attrib.get("href")
            cur_id = cur_title.attrib.get('id')
            # print ("{0}: {1}".format(cur_id, href)) 
            m = re.search(r"\#(\d+)\/(\-?\d+(?:\.\d+)?)\/(\-?\d+(?:\.\d+)?)$", href)
            if m:
                zoom, y, x = m.groups()
                zoom = int(zoom)
                y = float(y)
                x = float(x)
                d = {
                    'id': cur_id,
                    'zoom': zoom,
                    'y': y,
                    'x': x
                }
                titles.append(d)
    return titles
    # from pprint import pprint
    # pprint (titles)

ap = argparse.ArgumentParser("")
ap.add_argument("--input", type=argparse.FileType('r'), default=sys.stdin)
ap.add_argument("--output", type=argparse.FileType('w'), default=sys.stdout)
ap.add_argument("--title", default="scans", help="extract using this title")
args = ap.parse_args()
t = html5lib.parse(args.input, namespaceHTMLElements=False, treebuilder="etree")

tt = extract_markers(t, args.title)
print (json.dumps({'markers': tt}, indent=2), file=args.output)