
import webvtt, sys  
import argparse

ap = argparse.ArgumentParser("")
ap.add_argument("--input", type=argparse.FileType("r"), default=sys.stdin)
ap.add_argument("--href", default="")
ap.add_argument("--target", default=None)
ap.add_argument("--type", default=None)
args = ap.parse_args()

target=""
if args.target:
    target = " target=\"{}\"".format(args.target)
ttype=""
if args.type:
    ttype = " type=\"{}\"".format(args.type)

cc = webvtt.read_buffer(args.input)
for c in cc.captions:
    # print (c)
    print ("""<div class="caption">""")

    print ("""  <div class="timecode"><a href="{}#t={}"{}{}>{}</a></div>""".format(args.href, c.start, target, ttype, c.start))
    print ("""  <div class="lines">""")
    for line in c.lines:
        print ("""    <div class="line">{}</div>""".format(line))
    print ("""  </div>""")
    print ("""</div>""")
    print ()
