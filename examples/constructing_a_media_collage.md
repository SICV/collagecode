Constructing a media collage: An example using The Situationist Times
========================================================================= 

First of all let's add a div of iframes to receive each of the two "channels" of media (video + scans). Then style the page to put the iframes on the right hand third of the screen. Finally add a script to redirect links to open the media in (respective) frames.

```html
<div id="frames">
  <iframe id="video"></iframe>
  <iframe id="scans"></iframe>
</div>

<style>
body {
	padding-right: 33%;
}
#frames {
	position: fixed;
	top: 0px; right: 0px; bottom: 0px;
	width: 33%;
	display: flex;
	flex-direction: column;
}
#frames iframe {
	flex-grow: 1;
	width: 100%;
	border: 1px solid black;
}
</style>

<script>
var links = document.querySelectorAll("a"),
	frames = document.querySelectorAll("iframe"),
	frames_by_name = {};

for (var i=0,l=frames.length; i<l; i++) {
	let frame = frames[i];
	frames_by_name[frame.id] = frame;
}

for (var i=0,l=links.length; i<l; i++) {
	links[i].addEventListener("click", function (e) {
		// alert(`link: ${e.target.href}  (${e.target.title})`);
		e.preventDefault();
		frames_by_name[e.target.title].src = e.target.href;
	})
}
</script>
```

[This *almost* works](st01-01.cc.html). A couple things are not good though:

* Using an *iframe* for the video has disadvantages, particularly changing the fragment does *not* update the currentTime of the video. In addition, it's not possible to easily query the currentTime.
* The leaflet iframe does respond to changes in it's URL that move the fragment (it does however reload the entire page). Also, the connection is only "one-way" as the script can't access the remote fragment or respond to click on the markers.
* Clicks are currently triggering just single channels. Finally the desire is to let each paragraph function as a unit, activating all the links contained within it.

To improve the situation:

* Use a *video* element for the video track.
* Wrap the elements in *cc.frame*.
* Watch for hashchange events on the cc.frame. Adjust as necessary the active title.
* To get around the remote iframe issue: add a *tattletale* script to the remote leaflet page to map hashchanges into a posted window event (would using CORS on this server work as well?). In addition, map marker clicks to some kind of cc click event. Alternatively, moving the page to the same server would also work.

**Following hashchange events**

To do this efficiently, cc offers an index based on interval trees to quickly map a given fragment (in the case of video, a timecode) to a particular paragraph. Similarly, hash changes on the scans / leaflet page could be used to map positions onto titles.

To do this, the indexer needs to intelligently process the *sequence* of fragment links and creating intervals as appropriate. Other complexity: what if a playlist would have repetition of the same time code ... in this case a given timeupdate does not map to a single title ;o


maybe rather than doing interval trees... there was something good in sequential search based on the titles.

When looking up a novel position, calculate/imagine a smooth transition from whatever the current position is to that new position travelling the titles in (reverse) sequence... a bit like my old timeline code -- search from the current title either ascending or descending direction. In any case the *titles* are crucial that they remain the primary thing in indexing, so you query the *active playlist* for a given time or map position, and the index takes into account the current media positions (as defined by the active title, the current state of the linked media, or both?) to eventually jump to a new waypoint. 

Another view: a playlist defines a *virtual timeline* which an eventual scrubber control could be associated with. The challenge is then mapping individual media times to points on this virtual timeline.

Imagine a video that is annotated from start to finish twice in succession. Thus if the original media is 10 minutes long, the virtual timeline is twice that, or 20 minutes. Ideally, if a scrubber interface was available it would represent the full 20 minutes and any clicks / drags within that interface element is in principle unambiguous, you jump to that point in the presentation, and the linked media jumps to the appropriate point.

In allowing the media to drive the playlist, however, this flips the relationship where suddenly a change in the media timeline needs to be mapped to the virtual timeline.

Interesting cases: in ST, the annotations include *cut* markers that in a sense create gaps or holes in the mapping from the media timeline to the virtual.

Eventually, it's also interesting to consider dynamic playlists that might *not* be fully deterministic and thus able to create different virtual timelines depending on other interaction.

Does it help to differentiate between continuous and dis-continuous transitions when updating the active title?


